import React from 'react'
import { Link } from 'react-router-dom'
import ImageInput from './ImageInput'
import serialize from 'form-serialize'

class CreateContact extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault()
    const values = serialize(e.target, { hash: true})
    if (this.props.createContact) {
      this.props.createContact(values)
    }
  } 

    render() {
        return (
            <div>
              <Link 
                to='/'
                className='close-create-contact'
              >Back</Link>
              <form className='create-contact-form' onSubmit={this.handleSubmit}>
                <ImageInput 
                    name='avatarUrl'
                    className='create-contact-avatar-input'    
                    maxHeight={60}
                />
                <div className='create-contact-details'>
                  <input type='text' name='name' placeholder='Name'/>
                  <input type='text' name='email' placeholder='Email'/>
                  <button>Add Contact</button>
                </div>
               </form> 
            </div>
        )
    }
}

export default CreateContact