import React from 'react';
import { Route } from 'react-router-dom'
import PropTypes from 'prop-types';
import ContactList from './ContactList'
import * as ContactsAPI from './utils/ContactsAPI'
import CreateContact from './CreateContact';

class App extends React.Component {
  state = {
    contacts: []
  }

  componentDidMount() {
    ContactsAPI.getAll().then((contacts) => {
      this.setState({ contacts })
    })
  }

  /**
   * create new contact
   * 
   * @param {object} contact 
   */
  createContact(contact) {
    ContactsAPI.create(contact).then(contact => {
      this.setState(state => ({
        contacts: state.contacts.concat([contact])
      }))  
    })
  } 

  /**
   * remove contact
   * 
   * @param {object} contact 
   */
  removeContact = (contact) => {
    this.setState((state) => ({
      contacts: state.contacts.filter((c) => c.id !== contact.id)
    }))

    ContactsAPI.remove(contact)
  }

  render() {
    return (
      <div className="app">
          <Route exact path='/' render={() => (
            <ContactList 
              removeContact={this.removeContact} 
              contacts={this.state.contacts} 
            />  
          )} />
          <Route path='/create' render={({ history }) => (
            <CreateContact 
              createContact={(contact) => {
                this.createContact(contact)
                history.push('/') 
              }}
            />
          )} />
      </div>
    )
  }
}

ContactList.propTypes = {
  contacts: PropTypes.array.isRequired,
  removeContact: PropTypes.func.isRequired
}

export default App;
