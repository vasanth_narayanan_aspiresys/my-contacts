import React from 'react'
import { Link } from 'react-router-dom'
import escapeRegExp from 'escape-string-regexp'     
import sortBy from 'sort-by'

class ContactList extends React.Component {

    state = {
        query: ''
    }

    updateQuery = (query) => {
        this.setState({
            query: query.trim()
        })
    }

    clearQuery = () => {
        this.setState({ query: '' })
    }

    render() {
        
        const { contacts, removeContact } = this.props
        const { query } = this.state

        let showContacts
        if (query) {
            const match = new RegExp(escapeRegExp(query), 'i')
            showContacts = contacts.filter((contact) => match.test(contact.name))
        } else {
            showContacts = contacts
        }

        showContacts.sort(sortBy('name'))

        return (
            <div className="list-contacts">
                <div className="list-contacts-top">
                    <input 
                      type="text" 
                      className="search-contacts" 
                      placeholder="Search Contact"
                      value={this.state.query}
                      onChange={(event) => this.updateQuery(event.target.value) }  
                    />
                    <Link
                      to="/create"
                      className="add-contact"
                    >Add Contact</Link>
                </div>
                { showContacts.length !== contacts.length && 
                (<div className="showing-contacts">
                    <span>Now showing {showContacts.length} of {contacts.length}</span>
                    <button onClick={this.clearQuery}>Show All</button>
                </div> )}
                <ol className="contact-list">
                {showContacts.map((contact) => (
                    <li key={contact.id} className="contact-list-item">
                        <div className="contact-avatar" style={{
                            backgroundImage: `url(${contact.avatarURL})`
                        }}>
                        </div>
                        <div className="contact-details">
                            <p>{contact.name}</p>
                            <p>{contact.email}</p>
                        </div>
                        <button onClick={() => removeContact(contact)} className="contact-remove">
                            Remove
                        </button>
                    </li>
                ))}
                </ol>
            </div>
        )
    }    
}

export default ContactList